# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
from trytond.pool import Pool
from . import stock


def register():
    Pool.register(
        stock.LocationCrossReference,
        stock.Location,
        module='stock_location_cross_reference', type_='model')
    Pool.register(
        module='stock_location_cross_reference', type_='wizard')
    Pool.register(
        module='stock_location_cross_reference', type_='report')
