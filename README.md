datalife_stock_location_cross_reference
=======================================

The stock_location_cross_reference module of the Tryton application platform.

[![Build Status](http://drone.datalifeit.es:8050/api/badges/datalifeit/trytond-stock_location_cross_reference/status.svg)](http://drone.datalifeit.es:8050/datalifeit/trytond-stock_location_cross_reference)

Installing
----------

See INSTALL


License
-------

See LICENSE

Copyright
---------

See COPYRIGHT
